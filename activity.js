let users = `[
		    {
		      "userObjectId": "1",
		      "firstName": "Soren",
		      "lastName": "Kierkegaard",
		      "email": "whatisreal@kierkegaard.biz",
		      "password": "$omethingbrewing689",
		      "isAdmin": true,
		      "mobileNumber": "1-463-123-4447"
		    },
		    {
		      "userObjectId": "2",
		      "firstName": "Carl",
		      "lastName": "Jung",
		      "email": "mbti1sreal@cjung.info",
		      "password": "*y0mamasubconscious42o",
		      "isAdmin": false,
		      "mobileNumber": "(254)954-1289"
		    },
		    {
		      "userObjectId": "3",
		      "firstName": "Friedrich",
		      "lastName": "Nietzsche",
		      "email": "n0thingrlymatters@nietz.com",
		      "password": "g0d1sd3d&",
		      "isAdmin": true,
		      "mobileNumber": "024-648-3804"
		    }
		  ]`;
		  
		console.log(JSON.parse(users));


let orders = `[
		    {
		      "ordersObjectId": "1",
		      "userId": "3",
		      "transactionDate": "2023-03-01",
		      "status": "inactive",
		      "total": 20
		    },
		    {
		      "ordersObjectId": "2",
		      "userId": "1",
		      "transactionDate": "2022-01-01",
		      "status": "active",
		      "total": 300
		    },
		    {
		      "ordersObjectId": "3",
		      "userId": "2",
		      "transactionDate": "2023-04-22",
		      "status": "active",
		      "total": 10
		    }
		  ]`;
		  
		console.log(JSON.parse(orders));


let products = `[
		    {
		      "productsId": "1",
		      "name": "Lover",
		      "description": "Taylor Swift's album to her ex.",
		      "price": 100,
		      "stocks": 1000,
		      "isActive": false,
		      "sku": "lover-music-pop-001"
		    },
		    {
		      "productsId": "2",
		      "name": "Pretty Hate Machine",
		      "description": "If you're recently a NIN fan, you probably have watched that Black Mirror",
		      "price": 500,
		      "stocks": 10,
		      "isActive": true,
		      "sku": "phm-music-rock-005"
		    },
		    {
		      "productsId": "3",
		      "name": "Souled Out",
		      "description": "An album you'd want to listen to when you have anxiety attacks",
		      "price": 50,
		      "stocks": 300,
		      "isActive": true,
		      "sku": "souled-music-rnb-003"
		    }
		  ]`;
		  
		console.log(JSON.parse(products));

let orderProducts = `[
		    {
		      "orderProductsObjectId": "1",
		      "orderId": "2", 
		      "productId": "3",
		      "quantity": 300,
		      "price": 50,
		      "subTotal": 15000
		    },
		    {
		      "orderProductsObjectId": "2",
		      "orderId": "1",
		      "productId": "1",
		      "quantity": 20,
		      "price": 100,
		      "subTotal": 2000
		    },
		    {
		      "orderProductsObjectId": "3",
		      "orderId": "3",
		      "productId": "2",
		      "quantity": 10,
		      "price": 500,
		      "subTotal": 5000
		    }
		  ]`;
		  
		console.log(JSON.parse(orderProducts));